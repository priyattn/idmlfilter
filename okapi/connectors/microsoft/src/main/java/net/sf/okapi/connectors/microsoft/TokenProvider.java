package net.sf.okapi.connectors.microsoft;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.exceptions.OkapiException;

class TokenProvider {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final int TOKENRETRIES = 3;
	private int SLEEPPAUSE_MS = 300;
	private static final String TOKEN_URL = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";

	private HttpClient httpClient;
	private Parameters params;
	private DataMarketTokenParser.Token token = DataMarketTokenParser.expiredToken();

	public TokenProvider(HttpClient httpClient, Parameters params) {
		this.httpClient = httpClient;
		this.params = params;
	}

	public String get() {
		if (token.isExpired() || isExpiring()) {
			for (int tries = 0; tries < TOKENRETRIES; tries++) {
				if (getAccesstoken()) {
					return token.token;
				}
				else {
					if ( tries < TOKENRETRIES - 1 ) {
						sleep(SLEEPPAUSE_MS);
					}
				}
			}
			throw new OkapiException(String.format(
					"Failed to get Microsoft Translator access token after %d tries.",
					TOKENRETRIES));
		}
		return token.token;
	}

	// If less than half a second is left, let it expire
	private boolean isExpiring() {
		long now = System.currentTimeMillis();
		if (now > token.expiresOn - 500) {
			sleep(token.expiresOn - now);
			return true;
		}
		return false;
	}

	private void sleep(long ms) {
		try {
			Thread.sleep(ms);
		}
		catch ( InterruptedException e ) {
			throw new OkapiException("Sleep interrupted while attempting to get Azure Marketplace Token" + e.getMessage(), e);
		}
	}

	private boolean getAccesstoken () {
		try {
			String tokenRes = getRawToken();
			if ( tokenRes != null ) {
				token = new DataMarketTokenParser().parse(tokenRes);
				return true;
			}
		}
		catch ( OkapiException e ) {
			throw e;
		}
		catch (Exception e) {
			// Log and retry
			logger.error("Error in getAccestoken: {}", e.getMessage());
		}
		return false;
	}

	private String getRawToken() {
		HttpResponse response = null;
		try {
			HttpPost post = new HttpPost(TOKEN_URL);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
			nameValuePairs.add(new BasicNameValuePair("grant_type", "client_credentials"));
			nameValuePairs.add(new BasicNameValuePair("client_id", params.getClientId()));
			nameValuePairs.add(new BasicNameValuePair("client_secret", params.getSecret()));
			nameValuePairs.add(new BasicNameValuePair("scope", "http://api.microsofttranslator.com"));
			UrlEncodedFormEntity uefe = new UrlEncodedFormEntity(nameValuePairs);
			post.setEntity(uefe);
			response = httpClient.execute(post);
			if ( response != null ) {
				return StreamUtil.streamUtf8AsString(response.getEntity().getContent());
			}
		}
		catch (Exception e) {
			logger.error("Failed to fetch token", e);
		}
		finally {
            if (response != null) {
                try {
                    response.getEntity().getContent().close();
                }
                catch (IOException e) {}
            }
        }
		return null;
	}
}
