/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.simpletm;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {

	static final String DB_EXTENSION = ".h2.db";
	 /** 
	  * The full path of the database name to open.
	  * The path can have the extension {@link #DB_EXTENSION} or not extension.
	  */
	static final String DBPATH = "dbPath";
	static final String PENALIZETARGETWITHDIFFERENTCODES = "penalizeTargetWithDifferentCodes";
	static final String PENALIZESOURCEWITHDIFFERENTCODES = "penalizeSourceWithDifferentCodes";
	
	public Parameters () {
		super();
	}
	
	public Parameters (String initialData) {
		super(initialData);
	}
	
	public String getDbPath () {
		return getString(DBPATH);
	}

	public void setDbPath (String dbPath) {
		setString(DBPATH, dbPath);
	}
	
	public boolean getPenalizeTargetWithDifferentCodes () {
		return getBoolean(PENALIZETARGETWITHDIFFERENTCODES);
	}
	
	public void setPenalizeTargetWithDifferentCodes (boolean penalizeTargetWithDifferentCodes) {
		setBoolean(PENALIZETARGETWITHDIFFERENTCODES, penalizeTargetWithDifferentCodes);
	}
	
	public boolean getPenalizeSourceWithDifferentCodes () {
		return getBoolean(PENALIZESOURCEWITHDIFFERENTCODES);
	}
	
	public void setPenalizeSourceWithDifferentCodes (boolean penalizeSourceWithDifferentCodes) {
		setBoolean(PENALIZETARGETWITHDIFFERENTCODES, penalizeSourceWithDifferentCodes);
	}
	
	@Override
	public void reset () {
		super.reset();
		setDbPath("");
		setPenalizeSourceWithDifferentCodes(true);
		setPenalizeTargetWithDifferentCodes(true);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(DBPATH,
			"Path of the Database file",
			String.format("Full path of the database file (%s)", DB_EXTENSION));
		desc.add(PENALIZESOURCEWITHDIFFERENTCODES,
			"Penalize exact matches when the source has different codes than the query", null);
		desc.add(PENALIZETARGETWITHDIFFERENTCODES,
			"Penalize exact matches when the target has different codes than the query", null);
		return desc;
	}

}
