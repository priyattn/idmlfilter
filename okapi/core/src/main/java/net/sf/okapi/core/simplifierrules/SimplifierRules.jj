/*
  Okapi Simplifier Rules Parser
  Copyright (C) 2015 by the Okapi Framework contributors
  -----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
 
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
  SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
  RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
  USE OR PERFORMANCE OF THIS SOFTWARE.
*/
options{
  LOOKAHEAD=2;
  FORCE_LA_CHECK=false;  GENERATE_ANNOTATIONS=true; 
  GENERATE_CHAINED_EXCEPTION=true; 
  SUPPORT_CLASS_VISIBILITY_PUBLIC=true; 
  GENERATE_GENERICS=true; 
  GENERATE_STRING_BUILDER=true; 
  UNICODE_INPUT=true; 
  JAVA_UNICODE_ESCAPE=true; 
  ERROR_REPORTING = true;
  IGNORE_CASE = false;
  SANITY_CHECK = true;
  STATIC = false;
  KEEP_LINE_COLUMN = true;
  JDK_VERSION="1.7";}PARSER_BEGIN(SimplifierRules)package net.sf.okapi.core.simplifierrules;

import java.io.StringReader;

import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.Util;

public class SimplifierRules{
  private Code code;
    public static void main(String args [])  {    System.out.println("Reading from standard input...");    System.out.print("Enter an expression like \"if ADDABLE;\" :");    SimplifierRules parser = new SimplifierRules(System.in);    try    {      boolean r = parser.rules();    }    catch (Exception e)    {      System.out.println("Oops.");      System.out.println(e.getMessage());    }  }
  
  public final static void validate(String rules) throws ParseException {
    SimplifierRules r = new SimplifierRules(rules, new Code());
    r.parse();
  }
  
  public SimplifierRules()
  {   
  }
  
  public SimplifierRules(String input, Code code)
  {
    this(new StringReader(input));
    this.code = code;
  }
  
  public boolean evaluate(String input, Code code) throws ParseException
  {
    if (Util.isEmpty(input) || code == null) {
        return false;
    }
    
    ReInit(new StringReader(input));
    this.code = code;
    return parse();
  }
  
  public boolean parse() throws ParseException
  {
    return rules();
  }}PARSER_END(SimplifierRules)
// skip all whitespace and commentsSKIP :{  " "| "\t"| "\n"| "\r"| <"#" (~[ "\n", "\r" ])*    (      "\n"    | "\r"    | "\r\n"    )>| <"/*" (~[ "*" ])* "*"    (      ~[ "/" ] (~[ "*" ])* "*"    )*    "/">}

TOKEN :
{
  <RULE_START: "if">
| <RULE_END: ";"> 
}
// Code fields: data, outerData, originalId, type, TagType TOKEN :{  <DATA: "DATA">
| <OUTER_DATA: "OUTER_DATA">  
| <ORIGINAL_ID: "ORIGINAL_ID">
| <TYPE: "TYPE">
| <TAG_TYPE: "TAG_TYPE">}

// literals: CLOSING, OPENING, STANDALONE, addable, deletable or cloneableTOKEN :
{
  <CLOSING: "CLOSING">
| <OPENING: "OPENING">  
| <STANDALONE: "STANDALONE">
| <ADDABLE: "ADDABLE">
| <DELETABLE: "DELETABLE">
| <CLONEABLE: "CLONEABLE">
}

// boolean operators: or, andTOKEN :{  <OR  : "or">
| <AND : "and">
| <LPAREN: "(">
| <RPAREN: ")">}
// operators: =, ~, !=, !~
TOKEN :
{
  <EQUAL  : "=">
| <MATCH : "~">
| <NOT_EQUAL  : "!=">
| <NOT_MATCH : "!~">
}

// String tokens
TOKEN :
{
  <#QUOTE_DOUBLE : "\"">
| <STRING_DOUBLE_EMPTY : "\"\"">
| <#STRING_DOUBLE_BODY :
    (
      (~[ "\"", "\\", "\r", "\n", "\t" ])
    |
      (
        "\\"
        (
          "r"
        | "n"
        | "\\"
        | "\""
        | "t"
        )
      )
    )+>
| <STRING_DOUBLE_NONEMPTY : <QUOTE_DOUBLE> <STRING_DOUBLE_BODY> <QUOTE_DOUBLE>>
}

/**
    All rules are OR'ed with each other to get the final result.
    Short circuit and return early if any rule evaluates to true    
*/boolean rules() :{ boolean result=false; boolean r; }{  (<RULE_START> r=expression() <RULE_END> { if (r) return true; result |= r; })+ <EOF>
  { return result; }}boolean expression() :{ boolean result; boolean tail; }{    result=term() 
    (
        (<AND> tail=term() { result &= tail; } )  
        | 
        (<OR> tail=term() { result |= tail; })
    )*
    { return result; }}boolean term() :{ boolean result; TagType ctt; TagType ltt; String cs; String qs;}{  result=flagLiteral() { return result; }
  | 
  (ctt=codeTagTypeField() <EQUAL> ltt=tagTypeLiteral() { return ctt == ltt; }) 
  |
  (ctt=codeTagTypeField() <NOT_EQUAL> ltt=tagTypeLiteral() { return ctt != ltt; })
  | 
  (cs=codeString() <EQUAL> qs=queryString() { return cs.equals(qs); } )
  |
  (cs=codeString() <NOT_EQUAL> qs=queryString() { return !cs.equals(qs); })
  |
  (cs=codeString() <MATCH> qs=queryString() { return cs.matches(qs); })
  |
  (cs=codeString() <NOT_MATCH> qs=queryString() { return !cs.matches(qs); })
  | 
  (<LPAREN> result=expression() <RPAREN>) { return result; }}
String codeString() : {}{  <DATA> { return code.getData() == null ? "" : code.getData(); } 
  | 
  <OUTER_DATA> { return code.getOuterData() == null ? "" : code.getOuterData(); } 
  | 
  <ORIGINAL_ID> { return code.getOriginalId() == null ? "" : code.getOriginalId(); }
  | 
  <TYPE> { return code.getType() == null ? "" : code.getType(); }}

TagType codeTagTypeField() :
{}
{
  <TAG_TYPE> { return code.getTagType(); } 
}TagType tagTypeLiteral() :
{}{  <CLOSING> { return TagType.CLOSING; }
| <OPENING> { return TagType.OPENING; }  
| <STANDALONE> { return TagType.PLACEHOLDER; }
}

boolean flagLiteral() :
{}
{
 (<ADDABLE> { return code.isAdded(); }
| <DELETABLE> { return code.isDeleteable(); }
| <CLONEABLE> { return code.isCloneable(); })
}

String queryString() :
{Token t; }
{
(t=<STRING_DOUBLE_EMPTY> 
{ return ""; }  
| 
t=<STRING_DOUBLE_NONEMPTY>)
// remove quotes and unescape all escaped chars
{ return SimplifierRulesUtil.unescape(t.image.substring(1, t.image.length()-1)); }     
}
