package net.sf.okapi.common.exceptions;

/**
 * Signals that a filter has encountered an unexpected revision information.
 */
public class OkapiUnexpectedRevisionException extends OkapiBadFilterInputException {
}
