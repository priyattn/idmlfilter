package net.sf.okapi.common.resource;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_DELIMITER;
import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_PREFIX;
import static net.sf.okapi.common.resource.Code.EXTENDED_CODE_TYPE_VALUE_DELIMITER;

/**
 * Provides a code type builder.
 */
public class CodeTypeBuilder {
    private static final String EMPTY_VALUE = "";

    private boolean addExtendedCodeTypePrefix;

    private Set<String> codeTypes = new LinkedHashSet<>();
    private Map<String, String> codeTypesAndValues = new LinkedHashMap<>();

    public CodeTypeBuilder(boolean addExtendedCodeTypePrefix) {
        this.addExtendedCodeTypePrefix = addExtendedCodeTypePrefix;
    }

    public void addType(String type) {
        codeTypes.add(type);
    }

    public void addType(String type, String value) {
        codeTypesAndValues.put(type, value);
    }

    public String build() {
        if (codeTypes.isEmpty() && codeTypesAndValues.isEmpty()) {
            return EMPTY_VALUE;
        }

        StringBuilder codeTypeBuilder = new StringBuilder(addExtendedCodeTypePrefix ? EXTENDED_CODE_TYPE_PREFIX : EMPTY_VALUE);

        for (String codeType : codeTypes) {
            codeTypeBuilder.append(codeType).append(EXTENDED_CODE_TYPE_DELIMITER);
        }

        for (Map.Entry<String, String> codeTypeAndValue : codeTypesAndValues.entrySet()) {
            codeTypeBuilder.append(codeTypeAndValue.getKey()).append(EXTENDED_CODE_TYPE_VALUE_DELIMITER)
                    .append(codeTypeAndValue.getValue()).append(EXTENDED_CODE_TYPE_DELIMITER);
        }

        return codeTypeBuilder.toString();
    }
}
