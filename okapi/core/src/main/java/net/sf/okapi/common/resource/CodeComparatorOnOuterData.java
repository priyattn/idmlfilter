package net.sf.okapi.common.resource;

import java.util.Comparator;

import net.sf.okapi.common.StringUtil;


public class CodeComparatorOnOuterData implements Comparator<Code> {

	@Override
	public int compare(Code c1, Code c2) {	  
		// FIXME: remove whitespace because the XLIFF/TMX etc.. code data may be normalized
		String data1 = StringUtil.removeWhiteSpace(c1.getOuterData());
		String data2 = StringUtil.removeWhiteSpace(c2.getOuterData());
				
		if (data1.equals(data2) && c1.getTagType() == c2.getTagType()) {
			return 0;
		} else if (data1.equals(data2) && c1.getTagType() != c2.getTagType()) {
			return c1.getTagType().compareTo(c2.tagType); 
		} else {
			return data1.compareTo(data2);
		}
	}
}
