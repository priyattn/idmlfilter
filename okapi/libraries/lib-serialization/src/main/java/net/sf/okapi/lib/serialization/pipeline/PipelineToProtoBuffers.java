/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.serialization.pipeline;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.proto.pipeline.Pipeline;

public final class PipelineToProtoBuffers {
	
	static net.sf.okapi.proto.pipeline.Pipeline toPipeline(IPipeline okapiPipeline) {
		net.sf.okapi.proto.pipeline.Pipeline.Builder pipelineBuilder = net.sf.okapi.proto.pipeline.Pipeline.newBuilder();
		pipelineBuilder.setId(okapiPipeline.getId());
		for (IPipelineStep okapiStep : okapiPipeline.getSteps()) {
			net.sf.okapi.proto.pipeline.PipelineStep.Builder stepBuilder = net.sf.okapi.proto.pipeline.PipelineStep.newBuilder();
			stepBuilder.setName(okapiStep.getName());
			stepBuilder.setDescription(okapiStep.getDescription()); 
			stepBuilder.setStepClass(okapiStep.getClass().getName());
			IParameters p = okapiStep.getParameters();
			if (p != null) {
				stepBuilder.setParams(p.toString());
			}
			stepBuilder.setIsLastOutputStep(okapiStep.isLastOutputStep());
			pipelineBuilder.addSteps(stepBuilder);
		}
		return pipelineBuilder.build();
	}
	
	static byte[] toBytes(IPipeline okapiPipeline) {
		Pipeline p = toPipeline(okapiPipeline);
		return p.toByteArray();
	}
}
