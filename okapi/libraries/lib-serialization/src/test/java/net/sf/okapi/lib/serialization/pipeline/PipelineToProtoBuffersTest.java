package net.sf.okapi.lib.serialization.pipeline;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.pipeline.Pipeline;
import net.sf.okapi.lib.serialization.pipeline.PipelineToProtoBuffers;
import net.sf.okapi.lib.serialization.pipeline.ProtoBuffersToPipeline;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.createtarget.CreateTargetStep;
import net.sf.okapi.steps.common.removetarget.RemoveTargetStep;

public class PipelineToProtoBuffersTest {
	private Pipeline okapiPipeline;
	
	@Before
	public void setUp() {
		okapiPipeline = new Pipeline();
	}
	
	@After
	public void tearDown() throws Exception {
		okapiPipeline.destroy();
	}
	
	@Test
	public void emptyPipeline() {
		net.sf.okapi.proto.pipeline.Pipeline p = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertNotNull(p);
		assertTrue(p.getStepsCount() == 0);
		assertEquals(okapiPipeline.getId(), p.getId());
	}
	
	@Test
	public void multiStepsPipeline() {
		okapiPipeline.addStep(new FilterEventsToRawDocumentStep());
		okapiPipeline.addStep(new CreateTargetStep());
		okapiPipeline.addStep(new RemoveTargetStep());

		net.sf.okapi.proto.pipeline.Pipeline p = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertNotNull(p);
		assertTrue(p.getStepsCount() == 3);
		assertEquals(okapiPipeline.getId(), p.getId());

		assertEquals("Filter Events to Raw Document", p.getSteps(0).getName());
		assertEquals("Create Target", p.getSteps(1).getName());
		assertEquals("Remove Target", p.getSteps(2).getName());
		okapiPipeline.clearSteps();
	}
	
	@Test
	public void parameters() {
		IPipelineStep cts = new CreateTargetStep();
		IPipelineStep rts = new RemoveTargetStep();
		okapiPipeline.addStep(cts);
		okapiPipeline.addStep(rts);
		net.sf.okapi.proto.pipeline.Pipeline p = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertNotNull(p);
		assertEquals(cts.getParameters().toString(), p.getSteps(0).getParams());
		assertEquals(rts.getParameters().toString(), p.getSteps(1).getParams());
		okapiPipeline.clearSteps();
	}
	
	@Test
	public void stepClass() {
		IPipelineStep cts = new CreateTargetStep();
		IPipelineStep rts = new RemoveTargetStep();
		okapiPipeline.addStep(cts);
		okapiPipeline.addStep(rts);
		net.sf.okapi.proto.pipeline.Pipeline p = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertNotNull(p);
		assertEquals("net.sf.okapi.steps.common.createtarget.CreateTargetStep", p.getSteps(0).getStepClass());
		assertEquals("net.sf.okapi.steps.common.removetarget.RemoveTargetStep", p.getSteps(1).getStepClass());
		okapiPipeline.clearSteps();
	}
	
	@Test
	public void roundtrip() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		IPipelineStep cts = new CreateTargetStep();
		IPipelineStep rts = new RemoveTargetStep();
		okapiPipeline.addStep(cts);
		okapiPipeline.addStep(rts);
		net.sf.okapi.proto.pipeline.Pipeline p = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertNotNull(p);
		IPipeline rp = ProtoBuffersToPipeline.toPipeline(p);
		assertEquals("net.sf.okapi.steps.common.createtarget.CreateTargetStep", rp.getSteps().get(0).getClass().getName());
		assertEquals("net.sf.okapi.steps.common.removetarget.RemoveTargetStep", rp.getSteps().get(1).getClass().getName());		
		okapiPipeline.clearSteps();
	}
}
