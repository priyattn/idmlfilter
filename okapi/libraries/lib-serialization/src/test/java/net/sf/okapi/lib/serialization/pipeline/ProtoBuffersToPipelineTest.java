package net.sf.okapi.lib.serialization.pipeline;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.pipeline.IPipeline;
import net.sf.okapi.lib.serialization.pipeline.PipelineToProtoBuffers;
import net.sf.okapi.lib.serialization.pipeline.ProtoBuffersToPipeline;
import net.sf.okapi.proto.pipeline.Pipeline;
import net.sf.okapi.proto.pipeline.PipelineStep;
import net.sf.okapi.steps.common.createtarget.CreateTargetStep;
import net.sf.okapi.steps.common.removetarget.RemoveTargetStep;

public class ProtoBuffersToPipelineTest {
	private net.sf.okapi.proto.pipeline.Pipeline.Builder pipelineBuilder;
	private net.sf.okapi.proto.pipeline.PipelineStep.Builder stepBuilder;
	
	
	@Before
	public void setUp() {
		stepBuilder = net.sf.okapi.proto.pipeline.PipelineStep.newBuilder();
		pipelineBuilder = net.sf.okapi.proto.pipeline.Pipeline.newBuilder();
		pipelineBuilder.setId("ID");
		stepBuilder.setName("Create Target");
		stepBuilder.setDescription("");
		stepBuilder.setStepClass("net.sf.okapi.steps.common.createtarget.CreateTargetStep");
		stepBuilder.setParams((new CreateTargetStep()).getParameters().toString());
		PipelineStep cts = stepBuilder.build();
		pipelineBuilder.addSteps(cts);
		stepBuilder.clear();
		
		stepBuilder.setName("Remove Target");
		stepBuilder.setDescription("");
		stepBuilder.setStepClass("net.sf.okapi.steps.common.removetarget.RemoveTargetStep");
		stepBuilder.setParams((new RemoveTargetStep()).getParameters().toString());
		PipelineStep rts = stepBuilder.build();
		pipelineBuilder.addSteps(rts);
		stepBuilder.clear();
	}
	
	@After
	public void tearDown() throws Exception {
		stepBuilder.clear();
		pipelineBuilder.clear();
	}

	@Test
	public void emptyPipeline() throws InstantiationException, IllegalAccessException, ClassNotFoundException {		
		Pipeline protoPipeline = net.sf.okapi.proto.pipeline.Pipeline.newBuilder().build();	
		IPipeline okapiPipeline = ProtoBuffersToPipeline.toPipeline(protoPipeline);
		assertNotNull(okapiPipeline);
		assertTrue(okapiPipeline.getSteps().isEmpty());
		assertEquals(okapiPipeline.getId(), protoPipeline.getId());
	}
	
	@Test
	public void multiStepsPipeline() throws InstantiationException, IllegalAccessException, ClassNotFoundException {		
		Pipeline protoPipeline = pipelineBuilder.build();
		IPipeline okapiPipeline = ProtoBuffersToPipeline.toPipeline(protoPipeline);
		assertNotNull(okapiPipeline);
		assertTrue(okapiPipeline.getSteps().size() == 2);
		assertEquals(okapiPipeline.getId(), protoPipeline.getId());
		assertEquals("Create Target", okapiPipeline.getSteps().get(0).getName());
		assertEquals("Remove Target", okapiPipeline.getSteps().get(1).getName());
	}
	
	@Test
	public void parameters() throws InstantiationException, IllegalAccessException, ClassNotFoundException {		
		Pipeline protoPipeline = pipelineBuilder.build();
		IPipeline okapiPipeline = ProtoBuffersToPipeline.toPipeline(protoPipeline);
		assertNotNull(okapiPipeline);
		assertEquals(protoPipeline.getSteps(0).getParams(), okapiPipeline.getSteps().get(0).getParameters().toString());
		assertEquals(protoPipeline.getSteps(1).getParams(), okapiPipeline.getSteps().get(1).getParameters().toString());		
	}
	
	@Test
	public void stepClass() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Pipeline protoPipeline = pipelineBuilder.build();
		IPipeline okapiPipeline = ProtoBuffersToPipeline.toPipeline(protoPipeline);
		assertNotNull(okapiPipeline);
		assertEquals("net.sf.okapi.steps.common.createtarget.CreateTargetStep", okapiPipeline.getSteps().get(0).getClass().getName());
		assertEquals("net.sf.okapi.steps.common.removetarget.RemoveTargetStep", okapiPipeline.getSteps().get(1).getClass().getName());	
	}
	
	@Test
	public void roundtrip() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Pipeline protoPipeline = pipelineBuilder.build();
		IPipeline okapiPipeline = ProtoBuffersToPipeline.toPipeline(protoPipeline);
		assertNotNull(okapiPipeline);
		Pipeline rp = PipelineToProtoBuffers.toPipeline(okapiPipeline);
		assertEquals("net.sf.okapi.steps.common.createtarget.CreateTargetStep", rp.getSteps(0).getStepClass());
		assertEquals("net.sf.okapi.steps.common.removetarget.RemoveTargetStep", rp.getSteps(1).getStepClass());		
	}
}
