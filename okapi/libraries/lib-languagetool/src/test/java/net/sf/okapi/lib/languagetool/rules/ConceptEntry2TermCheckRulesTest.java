/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.languagetool.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.lib.terminology.ConceptEntry;
import net.sf.okapi.lib.terminology.csv.CSVReader;
import net.sf.okapi.lib.terminology.tbx.TBXReader;

public class ConceptEntry2TermCheckRulesTest {
	private static final LocaleId ENGLISH_US = new LocaleId("en-US");
	private static final LocaleId SPANISH = new LocaleId("es-ES");

	private ConceptEntry2TermCheckRules converter;
	private ConceptEntry2TermCheckRules converter2;
	private List<ConceptEntry> singleConcept;
	private List<ConceptEntry> multipleConcepts;

	@Before
	public void setUp() throws Exception {
		converter = new ConceptEntry2TermCheckRules(ENGLISH_US, SPANISH);
		converter2 = new ConceptEntry2TermCheckRules(ENGLISH_US, LocaleId.KOREAN);
		ConceptEntry sc = new ConceptEntry();
		sc.addTerm(ENGLISH_US, "term1 term2");
		sc.addTerm(SPANISH, "sterm1 sterm2");
		sc.addTerm(LocaleId.KOREAN, "kterm1 kterm2");
		singleConcept = new ArrayList<>();
		singleConcept.add(sc);

		ConceptEntry mc1 = new ConceptEntry();
		ConceptEntry mc2 = new ConceptEntry();
		mc1.addTerm(ENGLISH_US, "term1 term2");
		mc1.addTerm(SPANISH, "sterm1 sterm2");
		mc2.addTerm(ENGLISH_US, "new_term1 new_term2");
		mc2.addTerm(SPANISH, "snew_term1 snew_term2");
		multipleConcepts = new ArrayList<>();
		multipleConcepts.add(mc1);
		multipleConcepts.add(mc2);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void LanguageToolLocales() {
		assertEquals("English (US)", converter.getSrcLang().toString());
		assertEquals("Spanish", converter.getTrgLang().toString());
		assertEquals("Default", converter2.getTrgLang().toString());
	}

	@Test
	public void singleTermToBitext() throws IOException {
		List<TermCheckRule> rules = converter.convert(singleConcept);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 1);
		TermCheckRule r = rules.get(0);
		assertEquals("term1", r.getSrcRule().getPatternTokens().get(0).getString());
		assertEquals("term2", r.getSrcRule().getPatternTokens().get(1).getString());

		assertEquals("sterm1", r.getTrgRule().getPatternTokens().get(0).getString());
		assertEquals("sterm2", r.getTrgRule().getPatternTokens().get(1).getString());

		// using Default Language and tokenizer
		rules = converter2.convert(singleConcept);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 1);
		r = rules.get(0);
		assertEquals("term1", r.getSrcRule().getPatternTokens().get(0).getString());
		assertEquals("term2", r.getSrcRule().getPatternTokens().get(1).getString());

		assertEquals("kterm1", r.getTrgRule().getPatternTokens().get(0).getString());
		assertEquals("kterm2", r.getTrgRule().getPatternTokens().get(1).getString());
	}

	@Test
	public void multiTermToBitext() throws IOException {
		List<TermCheckRule> rules = converter.convert(multipleConcepts);
		assertTrue(!rules.isEmpty());
		assertTrue(rules.size() == 2);
		TermCheckRule r = rules.get(0);
		assertEquals("term1", r.getSrcRule().getPatternTokens().get(0).getString());
		assertEquals("term2", r.getSrcRule().getPatternTokens().get(1).getString());
		assertEquals("sterm1", r.getTrgRule().getPatternTokens().get(0).getString());
		assertEquals("sterm2", r.getTrgRule().getPatternTokens().get(1).getString());

		r = rules.get(1);
		assertEquals("new_term1", r.getSrcRule().getPatternTokens().get(0).getString());
		assertEquals("new_term2", r.getSrcRule().getPatternTokens().get(1).getString());
		assertEquals("snew_term1", r.getTrgRule().getPatternTokens().get(0).getString());
		assertEquals("snew_term2", r.getTrgRule().getPatternTokens().get(1).getString());
	}

	@Test
	public void tbxFile() throws IOException {
		ConceptEntry2TermCheckRules c = new ConceptEntry2TermCheckRules(LocaleId.ENGLISH, LocaleId.fromString("hu"));
		try (InputStream is = FileLocation.fromClass(this.getClass()).in("/test01.tbx").asInputStream()) {
			TBXReader reader = new TBXReader();
			reader.open(is);
			List<TermCheckRule> rules = c.convert(reader);
			assertTrue(!rules.isEmpty());
			assertTrue(rules.size() == 1);
		}
	}

	@Test
	public void csvFile() throws IOException {
		try (InputStream is = FileLocation.fromClass(this.getClass()).in("/test01.csv").asInputStream() ) {
			CSVReader reader = new CSVReader(ENGLISH_US, SPANISH);
			reader.open(is);
			List<TermCheckRule> rules = converter.convert(reader);
			assertTrue(!rules.isEmpty());
			assertTrue(rules.size() == 4);
		}
	}
}
