/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.writer;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filterwriter.XLIFFWriter;

/**
 * Extends the implementation of {@link XLIFFWriter} for XLIFF document
 * using a skeleton file made of serialized events.
 */
public class XLIFFAndSkeletonWriter extends XLIFFWriter {

	BeanEventWriter skeleton;
	
	public XLIFFAndSkeletonWriter () {
		super();
		skeleton = new BeanEventWriter();
	}
	
	@Override
	public Event handleEvent (Event event) {
		if ( event.isStartDocument() ) {
			// Remove the target only for monolingual formats
			boolean multiLing = event.getStartDocument().isMultilingual();
			((net.sf.okapi.lib.tkit.writer.Parameters)skeleton.getParameters()).setRemoveTarget(!multiLing);
		}
		// Call the skeleton first to avoid side-effects
		// like super.handleEvent(event) calling close() on END_DOCUEMT before the skeleton is done
		// Note that we need to use the returned value because the skeleton writer may change the TU
		// (like strip out the targets)
		event = skeleton.handleEvent(event);
		return super.handleEvent(event); 
	}

	@Override
	public void setOptions (LocaleId locale,
		String defaultEncoding)
	{
		super.setOptions(locale, defaultEncoding);
		skeleton.setOptions(locale, null); // Encoding is not used in skeleton
	}
	
	/**
	 * Sets the output path of both the XLIFF document and the skeleton file.
	 * The path of the skeleton file will be the same as the one specified for the XLIFF document,
	 * but with its extension replaced (or added) by '.skl'.
	 * Use {@link #setSkeletonOutput(String)} (after calling this method) to specify a different path
	 * for the skeleton file. 
	 * @param path the path of the XLIFF document.
	 */
	@Override
	public void setOutput (String path) {
		super.setOutput(path);
		int pos = path.lastIndexOf('.');
		if ( pos > -1 ) path = path.substring(0, pos);
		path += ".skl";
		skeleton.setOutput(path);
	}
	
	/**
	 * Sets the path of the skeleton file if it needs to be something else than the default
	 * set when calling {@link #setOutput(String)}.
	 * @param path the path to use for the skeleton file.
	 */
	public void setSkeletonOutput (String path) {
		skeleton.setOutput(path);
	}
	
	@Override
	public void close () {
		super.close();
		if ( skeleton != null ) {
			skeleton.close();
		}
	}

}
