/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.tkit.step;

import static net.sf.okapi.lib.tkit.step.MergerUtil.getTextUnitEvents;
import static net.sf.okapi.lib.tkit.step.MergerUtil.writeXliffAndSkeleton;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.steps.common.RawDocumentWriterStep;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SkeletonXliffMergerStepTest {
	private HtmlFilter htmlFilter;
	private String root;
	private SkeletonXliffMergerStep merger;
	private RawDocumentWriterStep writer;

	@Before
	public void setUp() {
		htmlFilter = new HtmlFilter();
		merger = new SkeletonXliffMergerStep();
		writer = new RawDocumentWriterStep();
		root = TestUtil.getParentDir(this.getClass(), "/dummy.txt");
	}

	@After
	public void tearDown() {
		htmlFilter.close();
		merger.destroy();
		writer.destroy();
	}

	@SuppressWarnings("resource")
	@Test
	public void simpleMerge() throws FileNotFoundException {
		String input = "simple.html";
		// Serialize the source file
		writeXliffAndSkeleton(FilterTestDriver.getEvents(
					htmlFilter, 
					new RawDocument(Util.toURI(root+input), "UTF-8", LocaleId.ENGLISH), null), 
				root, root+input+".xlf");

		merger.setOutputEncoding("UTF-8");
		merger.setSecondInput(new RawDocument(Util.toURI(root+input+".skl"),"UTF-8", LocaleId.ENGLISH));
		List<LocaleId> ts = new LinkedList<LocaleId>();
		ts.add(LocaleId.FRENCH);
		merger.setTargetLocales(ts);
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
						new RawDocument(Util.toURI(root+input+".xlf"), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH)));
		
		writer.setOutputURI(Util.toURI(root+input+".merged"));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(root+input), "UTF-8", LocaleId.ENGLISH);
		RawDocument trd = new RawDocument(Util.toURI(root+input+".merged"), "UTF-8", LocaleId.ENGLISH);
		List<Event> o = getTextUnitEvents(htmlFilter, ord);
		List<Event> t = getTextUnitEvents(htmlFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, false));
	}

	@Ignore("JSON skeleton merge may not be functional")
	public void testMergeWithNewSegments () {
		String input = "multi-sentences.xlf";
		// Extract the source document
		XLIFFFilter xlfFilter = new XLIFFFilter();
		net.sf.okapi.filters.xliff.Parameters params = xlfFilter.getParameters();
		List<Event> events = FilterTestDriver.getEvents(xlfFilter, 
			new RawDocument(Util.toURI(root+input), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH), null);
		// Serialize the source file
		writeXliffAndSkeleton(events, root, root+input+".xlf");

		// Create the segmentations
		xlfFilter = new XLIFFFilter();
		params = xlfFilter.getParameters();
		params.setOutputSegmentationType(net.sf.okapi.filters.xliff.Parameters.SEGMENTATIONTYPE_ASNEEDED);
		events = FilterTestDriver.getEvents(xlfFilter, 
			new RawDocument(Util.toURI(root+input+".xlf"), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH), null);
		
		
		merger = new SkeletonXliffMergerStep();
		merger.setParameters(params);
		merger.setOutputEncoding("UTF-8");
		merger.setSecondInput(new RawDocument(Util.toURI(root+input+".skl"),"UTF-8", LocaleId.ENGLISH));
		List<LocaleId> ts = new LinkedList<LocaleId>();
		ts.add(LocaleId.FRENCH);
		merger.setTargetLocales(ts);
		Event e = merger.handleEvent(new Event(EventType.RAW_DOCUMENT, 
			new RawDocument(Util.toURI(root+input+".xlf"), "UTF-8", LocaleId.ENGLISH, LocaleId.ENGLISH)));
		
		writer.setOutputURI(Util.toURI(root+input+".merged"));
		writer.handleEvent(e);
		writer.destroy();
		
		RawDocument ord = new RawDocument(Util.toURI(root+input), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		RawDocument trd = new RawDocument(Util.toURI(root+input+".merged"), "UTF-8", LocaleId.ENGLISH, LocaleId.FRENCH);
		List<Event> o = getTextUnitEvents(xlfFilter, ord);
		List<Event> t = getTextUnitEvents(xlfFilter, trd);
		assertTrue(o.size() == t.size());
		assertTrue(FilterTestDriver.compareEvents(o, t, false));
	}
}
