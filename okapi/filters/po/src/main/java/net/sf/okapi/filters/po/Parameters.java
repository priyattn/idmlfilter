/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.po;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {

	public static final String PROTECTAPPROVED = "protectApproved";
    private static final String ALLOWEMPTYTARGET = GenericSkeletonWriter.ALLOWEMPTYOUTPUTTARGET;
	private static final String BILINGUALMODE = "bilingualMode";
	private static final String MAKEID = "makeID";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String CODEFINDERRULES = "codeFinderRules";

	private InlineCodeFinder codeFinder;

	// POFilterWriter or filter-driven options, not persisted 
	private boolean wrapContent = true;
	private boolean outputGeneric = false;

	public Parameters () {
		super();
	}
	
	public boolean getBilingualMode () {
		return getBoolean(BILINGUALMODE);
	}

	public void setBilingualMode (boolean bilingualMode) {
		setBoolean(BILINGUALMODE, bilingualMode);
	}

	public boolean getProtectApproved () {
		return getBoolean(PROTECTAPPROVED);
	}

	public void setProtectApproved (boolean protectApproved) {
		setBoolean(PROTECTAPPROVED, protectApproved);
	}

	public boolean getUseCodeFinder () {
		return getBoolean(USECODEFINDER);
	}

	public void setUseCodeFinder (boolean useCodeFinder) {
		setBoolean(USECODEFINDER, useCodeFinder);
	}
	
	public InlineCodeFinder getCodeFinder () {
		return codeFinder;
	}

	public void setCodeFinder (InlineCodeFinder codeFinder) {
		this.codeFinder = codeFinder;
	}

	public boolean getMakeID () {
		return getBoolean(MAKEID);
	}
	
	public void setMakeID (boolean makeID) {
		setBoolean(MAKEID, makeID);
	}

	public boolean getWrapContent () {
		return wrapContent;
	}
	
	public void setWrapContent (boolean wrapContent) {
		this.wrapContent = wrapContent;
	}

	public boolean getOutputGeneric () {
		return outputGeneric;
	}
	
	public void setOutputGeneric (boolean outputGeneric) {
		this.outputGeneric = outputGeneric;
	}

	public boolean getAllowEmptyOutputTarget () {
		return getBoolean(ALLOWEMPTYTARGET);
	}
	
	public void setAllowEmptyOutputTarget (boolean allowEmptyOutputTarget) {
		setBoolean(ALLOWEMPTYTARGET, allowEmptyOutputTarget);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}

	public void reset () {
		super.reset();
		setBilingualMode(true);
		setMakeID(true);
		setProtectApproved(false);
		setUseCodeFinder(true);
		setAllowEmptyOutputTarget(false);

		codeFinder = new InlineCodeFinder();
		codeFinder.setSample("%s, %d, {1}, \\n, \\r, \\t, etc.");
		codeFinder.setUseAllRulesWhenTesting(true);
		// Default in-line codes: special escaped-chars and printf-style variable
		codeFinder.addRule("%(([-0+#]?)[-0+#]?)((\\d\\$)?)(([\\d\\*]*)(\\.[\\d\\*]*)?)[dioxXucsfeEgGpn]");
		codeFinder.addRule("(\\\\r\\\\n)|\\\\a|\\\\b|\\\\f|\\\\n|\\\\r|\\\\t|\\\\v");
		//TODO: Add Java-style variables. this is too basic
		codeFinder.addRule("\\{\\d[^\\\\]*?\\}");
		setSimplifierRules(null);
	}
	
	@Override
	public void fromString (String data) {
		super.fromString(data);
		codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
	}
	
	@Override
	public String toString () {
		buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
		return super.toString();
	}

}
