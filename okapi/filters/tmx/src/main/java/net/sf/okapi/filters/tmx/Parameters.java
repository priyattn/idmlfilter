/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.filters.tmx;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.encoder.XMLEncoder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {

	final static String SEGTYPE= "segType";
	final static String PROCESSALLTARGETS = "processAllTargets";
	final static String CONSOLIDATEDPSKELETON = "consolidateDpSkeleton";
	final static String EXITONINVALID = "exitOnInvalid";
	static final String PROPVALUESEP = "propValueSep";


	public Parameters () {
		super();
	}	

	public void reset() {
		super.reset();
		setEscapeGT(false);
		setProcessAllTargets(true);
		setConsolidateDpSkeleton(true);
		setExitOnInvalid(false);
		setSegType(TmxFilter.SEGTYPE_OR_SENTENCE);
		setPropValueSep(", ");
		setSimplifierRules(null);
	}
	
	public boolean getProcessAllTargets () {
		return getBoolean(PROCESSALLTARGETS);
	}

	public void setProcessAllTargets (boolean processAllTargets) {
		setBoolean(PROCESSALLTARGETS, processAllTargets);
	}

	public boolean getConsolidateDpSkeleton() {
		return getBoolean(CONSOLIDATEDPSKELETON);
	}

	public void setConsolidateDpSkeleton (boolean consolidateDpSkeleton) {
		setBoolean(CONSOLIDATEDPSKELETON, consolidateDpSkeleton);
	}

	public boolean getEscapeGT () {
		return getBoolean(XMLEncoder.ESCAPEGT);
	}

	public void setEscapeGT (boolean escapeGT) {
		setBoolean(XMLEncoder.ESCAPEGT, escapeGT);
	}

	public boolean getExitOnInvalid () {
		return getBoolean(EXITONINVALID);
	}

	public void setExitOnInvalid  (boolean exitOnInvalid) {
		setBoolean(EXITONINVALID, exitOnInvalid);
	}

	public int getSegType () {
		return getInteger(SEGTYPE);
	}
	
	public void setSegType (int segType) {
		setInteger(SEGTYPE, segType);
	}
	
	public String getPropValueSep() {
		return getString(PROPVALUESEP);
	}
	
	public void setPropValueSep(String sep) {
		setString(PROPVALUESEP, sep);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}
	
	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(XMLEncoder.ESCAPEGT, "Escape the greater-than characters", null);
		desc.add(PROCESSALLTARGETS, "Read all target entries", null);
		desc.add(CONSOLIDATEDPSKELETON, "Group all document parts skeleton into one", null);
		desc.add(EXITONINVALID, "Exit when encountering invalid <tu>s (default is to skip invalid <tu>s).", null);
		desc.add(SEGTYPE, "Creates or not a segment for the extracted <Tu>", null);
		desc.add(PROPVALUESEP, "String used to delimit property values when there are duplicate properties", null);
		return desc;
	}

}
