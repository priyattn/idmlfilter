/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.filters.openxml;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Class to parse ppt/presentations.xml (or other files of content type
 * application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml)
 * and resolve the embedded slide IDs to usable part names.
 */
public class Presentation {
	private XMLInputFactory factory;
	private Relationships rels;
	
	static final QName SLIDE_ID = Namespaces.PresentationML.getQName("sldId"); 
	
	private List<String> slidePartNames = new ArrayList<String>();
	
	public Presentation(XMLInputFactory factory, Relationships rels) {
		this.factory = factory;
		this.rels = rels;
	}
	
	public List<String> getSlidePartNames() {
		return slidePartNames;
	}
	
	public void parseFromXML(Reader reader) throws XMLStreamException {
		XMLEventReader eventReader = factory.createXMLEventReader(reader);
		
		while (eventReader.hasNext()) {
			XMLEvent e = eventReader.nextEvent();
			
			if (e.isStartElement()) {
				StartElement el = e.asStartElement();
				if (el.getName().equals(SLIDE_ID)) {
					Attribute id = el.getAttributeByName(Relationships.ATTR_REL_ID);
					if (id != null) {
						Relationships.Rel rel = rels.getRelById(id.getValue());
						if (rel == null) {
							throw new IllegalStateException(
									"Presentation refers to non-existent slide ID " + id.getValue());
						}
						slidePartNames.add(rel.target);
					}
				}
			}
		}
	}
}
