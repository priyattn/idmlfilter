package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;

abstract class ChunkParser<T> implements Parser<T> {
    protected StartElementContext startElementContext;
    protected IdGenerator nestedBlockIdGenerator;
    protected StyleDefinitions styleDefinitions;

    ChunkParser(StartElementContext startElementContext, IdGenerator nestedBlockIdGenerator, StyleDefinitions styleDefinitions) {
        this.startElementContext = startElementContext;
        this.nestedBlockIdGenerator = nestedBlockIdGenerator;
        this.styleDefinitions = styleDefinitions;
    }
}
