package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

class StringItem implements XMLEvents {
    private QName runName;
    private QName textName;
    private List<Chunk> chunks;


    StringItem(List<Chunk> chunks, QName runName, QName textName) {
        this.chunks = chunks;
        this.runName = runName;
        this.textName = textName;
    }

    /**
     * Return the QName of the element that contains run data in this block.
     */
    QName getRunName() {
        return runName;
    }

    /**
     * Return the QName of the element that contains text data in this block.
     */
    QName getTextName() {
        return textName;
    }

    @Override
    public List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>();
        for (XMLEvents chunk : chunks) {
            events.addAll(chunk.getEvents());
        }
        return events;
    }

    public List<Chunk> getChunks() {
        return chunks;
    }

    Block getBlock() {
        return new Block(chunks, runName, textName, false);
    }

    public boolean isStyled() {
        for (Chunk chunk: chunks) {
            if (chunk instanceof Run) {
                return true;
            }
        }
        return false;
    }
}
