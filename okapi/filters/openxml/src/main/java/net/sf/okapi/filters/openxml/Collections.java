package net.sf.okapi.filters.openxml;

import java.util.HashSet;
import java.util.Set;

/**
 * Provides collection handling methods.
 */
class Collections {

    /**
     * Creates a new hash set of provided items.
     *
     * @param items Items
     * @param <T>   A type of an item
     *
     * @return A hash set of provided items
     */
    @SafeVarargs
    static <T> Set<T> newHashSet(T... items) {
        Set<T> set = new HashSet<>();

        java.util.Collections.addAll(set, items);

        return set;
    }
}
