package net.sf.okapi.filters.openxml;

/**
 * Provides clarification parameters.
 */
class ClarificationParameters {

    private boolean shouldBeBidirectional;
    private boolean shouldEntailBidirectionalLang;
    private String bidirectionalLang;

    ClarificationParameters(boolean shouldBeBidirectional, boolean shouldEntailBidirectionalLang, String bidirectionalLang) {
        this.shouldBeBidirectional = shouldBeBidirectional;
        this.shouldEntailBidirectionalLang = shouldEntailBidirectionalLang;
        this.bidirectionalLang = bidirectionalLang;
    }

    boolean shouldBeBidirectional() {
        return shouldBeBidirectional;
    }

    boolean shouldEntailBidirectionalLang() {
        return shouldEntailBidirectionalLang;
    }

    String getBidirectionalLang() {
        return bidirectionalLang;
    }
}
