package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a markup.
 */
class Markup implements Chunk {
    private List<MarkupComponent> components = new ArrayList<>();

    @Override
    public List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>();

        for (MarkupComponent component : components) {
            events.addAll(component.getEvents());
        }

        return events;
    }

    Markup addComponent(MarkupComponent component) {
        components.add(component);

        return this;
    }

    List<MarkupComponent> getComponents() {
        return components;
    }

    Nameable getNameableMarkupComponent() {
        for (MarkupComponent markupComponent : components) {
            if (markupComponent instanceof Nameable) {
                return (Nameable) markupComponent;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + components.size() + ") " + components;
    }
}