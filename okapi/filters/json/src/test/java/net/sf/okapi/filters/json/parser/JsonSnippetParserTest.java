/*===========================================================================
  Copyright (C) 2009-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.json.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JsonSnippetParserTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSingleObject() throws IOException, ParseException {
		String snippet = "/*comment*/ //comment\n#comment\n         \"zerokey:\"  {    \n               \"key\":\"value\u0067\"}";
		JsonParser jp = new JsonParser(snippet);
		Token t;
		do {
			t = jp.getNextToken();			
		} while (t.kind != 0);
	}

	@Test
	public void testTwoObjects() throws IOException, ParseException {
	}
	
	 private String getSpecialTokenBefore(Token t)
	  {
		    List<Token> tl = new ArrayList<Token>();

	    StringBuilder b = new StringBuilder();
	    if (t.specialToken == null) return null;
	    // The above statement determines that there are no special tokens
	    // and returns control to the caller.
	    Token tmp_t = t.specialToken;
	    while (tmp_t.specialToken != null) tmp_t = tmp_t.specialToken;
	    // The above line walks back the special token chain until it
	    // reaches the first special token after the previous regular
	    // token.
	    while (tmp_t != null)
	    {
	      b.append(tmp_t.kind+tmp_t.image);
	      tmp_t = tmp_t.next;
	    }
	    // The above loop now walks the special token chain in the forward
	    // direction printing them in the process.
	    return b.toString();
	  }
}
