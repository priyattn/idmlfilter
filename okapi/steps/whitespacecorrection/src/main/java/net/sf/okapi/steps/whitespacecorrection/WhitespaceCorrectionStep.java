/*===========================================================================
  Copyright (C) 2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.whitespacecorrection;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;

/**
 * Provides a general whitespace correction step.
 */
@UsingParameters(WhitespaceCorrectionStepParameters.class)
public class WhitespaceCorrectionStep extends BasePipelineStep {
    protected LocaleId sourceLocale;
    protected LocaleId targetLocale;
    private WhitespaceCorrectionStepParameters params = new WhitespaceCorrectionStepParameters();

    @Override
    public String getName() {
        return "Whitespace Correction";
    }

    @Override
    public String getDescription() {
        return "Correct whitespace following segment-ending punctuation when translating from " +
               " a space-delimited language (such as English) to a non-space-delimited language " +
               "(Chinese or Japanese), or vice-versa. " +
               "Expects: filter events. Sends back: filter events.";
    }

    @Override
    public WhitespaceCorrectionStepParameters getParameters() {
        return params;
    }

    @Override
    public void setParameters (IParameters params) {
        this.params = (WhitespaceCorrectionStepParameters)params;
    }

    @StepParameterMapping(parameterType = StepParameterType.SOURCE_LOCALE)
    public void setSourceLocale(LocaleId sourceLocale) {
        this.sourceLocale = sourceLocale;
    }

    @StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
    public void setTargetLocale(LocaleId targetLocale) {
        this.targetLocale = targetLocale;
    }

    @Override
    protected Event handleTextUnit(Event event) {
        new WhitespaceCorrector(sourceLocale, targetLocale,
                        getParameters().getPunctuation()).correctWhitespace(event.getTextUnit());
        return event;
    }
}
