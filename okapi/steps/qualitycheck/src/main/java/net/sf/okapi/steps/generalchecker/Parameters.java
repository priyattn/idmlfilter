/*===========================================================================
 Copyright (C) 2016 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.generalchecker;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	  private static final String LEADINGWS = "leadingWS";
	  private static final String TRAILINGWS = "trailingWS";
	  private static final String EMPTYTARGET = "emptyTarget";
	  private static final String EMPTYSOURCE = "emptySource";
	  private static final String TARGETSAMEASSOURCE = "targetSameAsSource";
	  private static final String TARGETSAMEASSOURCE_FORSAMELANGUAGE = "targetSameAsSourceForSameLanguage";
	  private static final String TARGETSAMEASSOURCE_WITHCODES = "targetSameAsSourceWithCodes";
	  private static final String DOUBLEDWORD = "doubledWord";
	  private static final String DOUBLEDWORDEXCEPTIONS = "doubledWordExceptions";

	public Parameters() {
		super();
	}	

	public boolean getDoubledWord() {
		return getBoolean(DOUBLEDWORD);
	}

	public void setDoubledWord(boolean doubledWord) {
		setBoolean(DOUBLEDWORD, doubledWord);
	}

	public String getDoubledWordExceptions() {
		return getString(DOUBLEDWORDEXCEPTIONS);
	}

	public void setDoubledWordExceptions(String doubledWordExceptions) {
		setString(DOUBLEDWORDEXCEPTIONS, doubledWordExceptions);
	}

	public boolean getLeadingWS() {
		return getBoolean(LEADINGWS);
	}

	public void setLeadingWS(boolean leadingWS) {
		setBoolean(LEADINGWS, leadingWS);
	}

	public boolean getTrailingWS() {
		return getBoolean(TRAILINGWS);
	}

	public void setTrailingWS(boolean trailingWS) {
		setBoolean(TRAILINGWS, trailingWS);
	}

	public boolean getEmptyTarget() {
		return getBoolean(EMPTYTARGET);
	}

	public void setEmptyTarget(boolean emptyTarget) {
		setBoolean(EMPTYTARGET, emptyTarget);
	}

	public boolean getEmptySource() {
		return getBoolean(EMPTYSOURCE);
	}

	public void setEmptySource(boolean emptySource) {
		setBoolean(EMPTYSOURCE, emptySource);
	}

	public boolean getTargetSameAsSource() {
		return getBoolean(TARGETSAMEASSOURCE);
	}

	public void setTargetSameAsSource(boolean targetSameAsSource) {
		setBoolean(TARGETSAMEASSOURCE, targetSameAsSource);
	}

	public boolean getTargetSameAsSourceForSameLanguage() {
		return getBoolean(TARGETSAMEASSOURCE_FORSAMELANGUAGE);
	}

	public void setTargetSameAsSourceForSameLanguage(boolean targetSameAsSourceForSameLanguage) {
		setBoolean(TARGETSAMEASSOURCE_FORSAMELANGUAGE, targetSameAsSourceForSameLanguage);
	}

	public boolean getTargetSameAsSourceWithCodes() {
		return getBoolean(TARGETSAMEASSOURCE_WITHCODES);
	}

	public void setTargetSameAsSourceWithCodes(boolean targetSameAsSourceWithCodes) {
		setBoolean(TARGETSAMEASSOURCE_WITHCODES, targetSameAsSourceWithCodes);
	}
	
	@Override
	public void reset() {
		super.reset();	
		setLeadingWS(true);
		setTrailingWS(true);
		setEmptyTarget(true);
		setEmptySource(true);
		setTargetSameAsSource(true);
		setTargetSameAsSourceForSameLanguage(true);
		setTargetSameAsSourceWithCodes(true);
		setDoubledWord(true);
		setDoubledWordExceptions("sie;vous;nous");
	}

	@Override
	public void fromString(String data) {
		super.fromString(data);
	}

	@Override
	public String toString() {		
		return super.toString();
	}
}
